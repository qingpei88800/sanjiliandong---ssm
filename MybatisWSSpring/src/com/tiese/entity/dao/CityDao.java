package com.tiese.entity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.tiese.entity.City;

public interface CityDao {
	@Select(value="select * from city")
	public List<City> getAllCity();

	public City getCityByID(int id);
}
