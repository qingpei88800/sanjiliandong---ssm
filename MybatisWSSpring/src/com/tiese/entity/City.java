package com.tiese.entity;

import java.util.List;

public class City {
	private int id;
	private String cityID;
	private String city;
	private String father;
	private List<Area> areas;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCityID() {
		return cityID;
	}

	public void setCityID(String cityID) {
		this.cityID = cityID;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFather() {
		return father;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public List<Area> getAreas() {
		return areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	@Override
	public String toString() {
		return "City [id=" + id + ", cityID=" + cityID + ", city=" + city + ", father=" + father + ", areas=" + areas
				+ "]";
	}

}
