package com.tiese.service;

import java.util.List;

import com.tiese.entity.Area;

public interface AreaService {
	public void insertArea(Area area);

	public void updateArea(Area area);

	public void deleteArea(int id);

	public List<Area> findAllArea();

	public Area findAreaByID(int id);
	
	public List<Area> findAreaByFather(int father);
}
