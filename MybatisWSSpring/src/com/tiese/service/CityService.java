package com.tiese.service;

import java.util.List;

import com.tiese.entity.City;

public interface CityService {
	public void insertCity(City city);

	public void updateCity(City city);

	public void deleteCity(int id);

	public List<City> findAllCity();
	public List<City> findCityByFather(int father);
	public City findCityByID(int id);
}
