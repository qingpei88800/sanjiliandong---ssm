package com.tiese.service;

import java.util.List;

import com.tiese.entity.Province;

public interface ProvinceService {
	public void insertProvince(Province province);

	public void updateProvince(Province province);

	public void deleteProvince(int id);

	public List<Province> findAllProvince();

	public Province findProvinceByID(int id);
}
