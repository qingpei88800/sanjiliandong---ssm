package com.tiese.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Service;

import com.tiese.entity.Province;
import com.tiese.service.ProvinceService;

@Service
public class ProvinceServiceImpl implements ProvinceService {
	@Resource
	private SqlSessionFactory sqlSessionFactory;

	@Override
	public void insertProvince(Province province) {
		sqlSessionFactory.openSession().insert("insertProvince", province);

	}

	@Override
	public void updateProvince(Province province) {
		sqlSessionFactory.openSession().update("updateProvince", province);

	}

	@Override
	public void deleteProvince(int id) {
		sqlSessionFactory.openSession().delete("deleteProvince", id);

	}

	@Override
	public List<Province> findAllProvince() {
		return sqlSessionFactory.openSession().selectList("getAllProvince");
	}

	@Override
	public Province findProvinceByID(int id) {
		Province province = sqlSessionFactory.openSession().selectOne("getProvinceByid", id);
		return province;
	}

	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

}
