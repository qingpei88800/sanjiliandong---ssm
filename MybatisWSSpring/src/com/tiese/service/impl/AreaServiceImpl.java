package com.tiese.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Service;

import com.tiese.entity.Area;
import com.tiese.service.AreaService;

@Service
public class AreaServiceImpl implements AreaService {
	@Resource
	private SqlSessionFactory sqlSessionFactory;

	@Override
	public void insertArea(Area area) {
		sqlSessionFactory.openSession().insert("insertArea", area);

	}

	@Override
	public void updateArea(Area area) {
		sqlSessionFactory.openSession().update("updateArea", area);

	}

	@Override
	public void deleteArea(int id) {
		sqlSessionFactory.openSession().delete("deleteArea", id);

	}

	@Override
	public List<Area> findAllArea() {
		return sqlSessionFactory.openSession().selectList("getAllArea");
	}

	@Override
	public Area findAreaByID(int id) {
		Area area = sqlSessionFactory.openSession().selectOne("getAreaByid", id);
		return area;
	}

	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	@Override
	public List<Area> findAreaByFather(int father) {
		List<Area> areas = sqlSessionFactory.openSession().selectList("getAreaByFather",father);
		return areas;
	}
}
