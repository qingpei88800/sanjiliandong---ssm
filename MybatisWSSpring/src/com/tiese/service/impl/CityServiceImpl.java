package com.tiese.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Service;

import com.tiese.entity.City;
import com.tiese.service.CityService;

@Service
public class CityServiceImpl implements CityService {
	@Resource
	private SqlSessionFactory sqlSessionFactory;

	@Override
	public void insertCity(City city) {
		sqlSessionFactory.openSession().insert("insertCity", city);

	}

	@Override
	public void updateCity(City city) {
		sqlSessionFactory.openSession().update("updateCity", city);

	}

	@Override
	public void deleteCity(int id) {
		sqlSessionFactory.openSession().delete("deleteCity", id);

	}

	@Override
	public List<City> findAllCity() {
		return sqlSessionFactory.openSession().selectList("getAllCity");
	}

	@Override
	public City findCityByID(int id) {
		City city = sqlSessionFactory.openSession().selectOne("getCityByid", id);
		return city;
	}

	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	@Override
	public List<City> findCityByFather(int father) {
		List<City>  cities =sqlSessionFactory.openSession().selectList("getCityByFather",father);
		return cities;
	}
}
