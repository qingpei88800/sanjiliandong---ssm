package com.tiese.controller;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tiese.entity.Area;
import com.tiese.entity.City;
import com.tiese.entity.Province;
import com.tiese.service.AreaService;
import com.tiese.service.CityService;
import com.tiese.service.ProvinceService;

@Controller
public class WebController {

	public WebController() {
		System.out.println("1111111111111111111111111111111111111");
	}

	@Autowired
	private ProvinceService provinceServiceImpl;
	@Autowired
	private CityService cityServiceImpl;
	@Autowired
	private AreaService areaServiceImpl;
	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	@RequestMapping("/getProvinces.do")
	public @ResponseBody List<Province> requestJson() {
		System.out.println("requestJson ��������");
		List<Province> list = provinceServiceImpl.findAllProvince();
		return list;

	}

	@RequestMapping("/getCities.do")
	@ResponseBody
	public List<City> testMVC(@RequestParam(value = "father") int father) {
		List<City> cities = cityServiceImpl.findCityByFather(father);
		return cities;
	}

	@RequestMapping("/getAreas.do")
	@ResponseBody
	public List<Area> testMVC1(@RequestParam(value = "father") int father) {
		List<Area> areas = areaServiceImpl.findAreaByFather(father);
		return areas;
	}

	public ProvinceService getProvinceServiceImpl() {
		return provinceServiceImpl;
	}

	public void setProvinceServiceImpl(ProvinceService provinceServiceImpl) {
		this.provinceServiceImpl = provinceServiceImpl;
	}

	public CityService getCityServiceImpl() {
		return cityServiceImpl;
	}

	public void setCityServiceImpl(CityService cityServiceImpl) {
		this.cityServiceImpl = cityServiceImpl;
	}

	public AreaService getAreaServiceImpl() {
		return areaServiceImpl;
	}

	public void setAreaServiceImpl(AreaService areaServiceImpl) {
		this.areaServiceImpl = areaServiceImpl;
	}

	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

}
