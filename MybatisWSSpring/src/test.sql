 1 CREATE TABLE teacher(
 2     t_id INT PRIMARY KEY AUTO_INCREMENT, 
 3     t_name VARCHAR(20)
 4 );
 5 CREATE TABLE class(
 6     c_id INT PRIMARY KEY AUTO_INCREMENT, 
 7     c_name VARCHAR(20), 
 8     teacher_id INT
 9 );
 
 CREATE TABLE student(
    s_id INT PRIMARY KEY AUTO_INCREMENT, 
    s_name VARCHAR(20), 
    class_id INT
);