<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <% 
    	String path = request.getContextPath();
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="<%=path %>/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$().ready(
		function(){
			 $.get({
					url:"<%=path%>/getProvinces.do",
					success:function(data){
						var html="<option >---请选择---</option>";
						for(var i =0 ; i< data.length;i++){
							html+="<option id='"+data[i].provinceID+"'>"+data[i].province+"</option>";
					};
					$("#province").html(html);
					}
				 });
		}
		);
function changeEvent(data){
	$.get({
		url:"<%=path%>/getCities.do",
		data:"father="+ data,
		success:function(data){
			var html="<option >---请选择---</option>";
			for(var i =0 ; i< data.length;i++){
				html+="<option id='"+data[i].cityID+"'>"+data[i].city+"</option>";
		};
		$("#city").html(html);
		}
	});
}

function changeEvent2(data){
	$.get({
		url:"<%=path%>/getAreas.do",
		data:"father="+ data,
		success:function(data){
			var html="<option >---请选择---</option>";
			for(var i =0 ; i< data.length;i++){
				html+="<option id='"+data[i].areaId+"'>"+data[i].area+"</option>";
		};
		$("#area").html(html);
		}
	});
}
 
</script>
</head>
<body>
	<h1>你好中国人</h1>
	
	<div>
	省份：
		<select id="province" onchange="changeEvent(this.options[this.options.selectedIndex].id);">
			
		</select>
	区市：<select id="city" onchange="changeEvent2(this.options[this.options.selectedIndex].id);"><option>---请选择---</option></select>
	县市：<select id="area">
				<option>---请选择---</option>
			</select>
	</div>

</body>
</html>